# Sisop Modul 4 <br> Kelompok ITA 26



## Soal 1

### Cara Kerja
Kode yang Anda berikan adalah program dalam bahasa C yang membaca file CSV ("FIFA23_official_data.csv") dan mencetak data pemain yang memenuhi kriteria tertentu. Mari kita jelaskan bagaimana kode ini bekerja secara detail.

1. Pada awal program, beberapa header file termasuk <stdio.h>, <stdlib.h>, dan <string.h> diimpor. Ini diperlukan untuk menggunakan fungsi standar input/output, alokasi memori, dan fungsi string.

2. Program utama dimulai dengan membuka file "FIFA23_official_data.csv" menggunakan fungsi `fopen()`. Jika file tidak dapat ditemukan atau dibuka, program akan mencetak pesan kesalahan dan mengembalikan nilai 1 untuk menandakan kegagalan.

3. Sebuah array karakter `line` dengan ukuran 1000 dideklarasikan. Ini akan digunakan untuk menyimpan baris-baris data dari file CSV.

4. Fungsi `fgets()` digunakan untuk membaca baris header pertama dari file CSV dan mengabaikannya. Baris ini berisi label kolom, yang tidak akan dicetak.

5. Program mencetak header untuk data pemain yang akan dicetak, yaitu "Nama Pemain | Klub | Umur | Potensi | URL Foto".

6. Selama ada baris yang dapat dibaca dari file CSV, program akan memasukkan data dari setiap kolom ke dalam variabel yang sesuai menggunakan fungsi `strtok()` dan `switch-case`.

7. Fungsi `strtok()` digunakan untuk membagi baris menjadi token-token yang dipisahkan oleh koma. Variabel `token` menunjuk ke token pertama dalam baris, dan loop `while` berjalan sampai tidak ada token lagi.

8. Dalam loop `while`, setiap token dimasukkan ke variabel yang sesuai berdasarkan nilai `column` menggunakan `switch-case`. Variabel-variabel ini akan menyimpan data pemain seperti nama, klub, umur, potensi, dan URL foto.

9. Setelah memasukkan token ke variabel yang sesuai, variabel `token` diupdate dengan token berikutnya menggunakan `strtok(NULL, ",")`, dan `column` juga ditingkatkan.

10. Setelah selesai membaca semua token dalam baris, program memeriksa apakah data pemain memenuhi kriteria tertentu. Kriteria ini adalah: umur kurang dari 25, potensi lebih besar dari 85, dan klub bukan "Manchester City". Jika pemain memenuhi kriteria tersebut, data pemain dicetak menggunakan `printf()`.

11. Setelah selesai membaca semua baris dalam file CSV, program menutup file menggunakan `fclose()`.

12. Program mengembalikan nilai 0 untuk menandakan keberhasilan dan keluar.

Jadi, keseluruhan kode ini membaca file CSV, memproses data pemain, dan mencetak data pemain yang memenuhi kriteria tertentu.

### Screenshot File
![Semantic description of image](/Foto/File_1.png "Image Title")

### Screenshot Kode
![Semantic description of image](/Foto/Kode1_1.png "Image Title")
![Semantic description of image](/Foto/Kode1_2.png "Image Title")

### Screenshot Program
![Semantic description of image](/Foto/1.png "Image Title")

### Kendala Pengerjaan
- belum bisa download data set untuk storage.c
- tidak bisa run program karena docker masih belum terpasang



## Soal 4

### Cara Kerja

Kode yang Anda berikan adalah program dalam bahasa C yang memiliki beberapa fungsi untuk memanipulasi file dan direktori. Mari kita jelaskan bagaimana kode ini bekerja secara detail.

1. Pada awal program, beberapa header file termasuk <stdio.h>, <stdlib.h>, <string.h>, <unistd.h>, <fcntl.h>, <dirent.h>, <sys/stat.h>, <sys/types.h>, dan <time.h> diimpor. Ini diperlukan untuk menggunakan fungsi standar input/output, alokasi memori, operasi file, dan operasi sistem.

2. Dalam program ini, terdapat beberapa konstanta yang didefinisikan menggunakan `#define`. Konstanta pertama adalah `MODULE_PREFIX` yang berisi string "module_". Konstanta ini digunakan untuk memeriksa apakah suatu direktori menjadi modular atau tidak. Konstanta kedua adalah `CHUNK_SIZE` yang memiliki nilai 1024. Konstanta ini digunakan untuk menentukan ukuran setiap chunk saat membuat file dalam bentuk chunked.

3. Program memiliki fungsi `create_log()` yang digunakan untuk membuat entri log ke file "fs_module.log". Fungsi ini menerima tiga argumen, yaitu `cmd` (perintah), `desc` (deskripsi), dan `log_file` (nama file log). Fungsi ini menggunakan fungsi `fopen()` untuk membuka file log dalam mode "a" (menambahkan pada akhir file). Jika file berhasil dibuka, fungsi `fprintf()` digunakan untuk menulis entri log ke file. Terakhir, fungsi `fclose()` digunakan untuk menutup file log.

4. Fungsi `create_chunked_file()` digunakan untuk membagi file yang diberikan menjadi chunked files. Fungsi ini menerima argumen `file_path` yang merupakan path file asli yang akan di-chunked. Pertama, fungsi membuka file asli menggunakan `fopen()` dalam mode "rb" (baca file dalam mode biner). Jika file asli berhasil dibuka, fungsi menggunakan `struct stat` untuk mendapatkan informasi tentang file tersebut, khususnya ukuran file. Kemudian, fungsi memulai loop untuk membaca file asli dan membuat chunked files. Setiap chunked file diberi nama berdasarkan nomor urut (misalnya, file_path.000, file_path.001, dll.). Fungsi membaca sejumlah data dari file asli, menulis data tersebut ke chunked file, dan mengurangi jumlah byte yang tersisa dari file asli. Fungsi ini juga mencatat jumlah total chunked files yang dibuat. Setelah selesai membaca file asli, file asli ditutup menggunakan `fclose()` dan dihapus menggunakan `remove()`. Terakhir, fungsi `create_log()` dipanggil untuk mencatat ke log bahwa file telah di-chunked.

5. Fungsi `restore_original_file()` digunakan untuk mengembalikan file asli dari chunked files. Fungsi ini menerima argumen `file_path` yang merupakan path file asli yang ingin dikembalikan. Pertama, fungsi membentuk nama file asli berdasarkan `file_path` dengan menambahkan ekstensi ".000" ke belakangnya. Fungsi membuka file asli menggunakan `fopen()` dalam mode "wb" (tulis file dalam mode biner). Jika file asli berhasil dibuka, fungsi

### Screenshot File
![Semantic description of image](/Foto/File2.png "Image Title")

### Screenshot Kode
![Semantic description of image](/Foto/Kode2_1.png "Image Title")
![Semantic description of image](/Foto/Kode2_2.png "Image Title")
![Semantic description of image](/Foto/Kode2_3.png "Image Title")
![Semantic description of image](/Foto/Kode2_4.png "Image Title")
![Semantic description of image](/Foto/Kode2_5.png "Image Title")

### Screenshot Program
![Semantic description of image](/Foto/4.png "Image Title")

### Kendala Pengerjaan
- masalah pada RENAME
- tidak bisa mengubah file menjadi modular



## Soal 5

### Cara Kerja
Kode yang diberikan adalah implementasi dari sebuah file sistem virtual menggunakan FUSE (Filesystem in Userspace). FUSE memungkinkan pengembang untuk membuat file sistem yang dapat diakses dan dioperasikan oleh pengguna di ruang pengguna tanpa memerlukan akses ke kernel atau modul kernel.

Berikut adalah penjelasan tentang cara kerja kode ini:

1. Mendefinisikan Beberapa Konstanta
   - `MAX_USERNAME_LENGTH` dan `MAX_PASSWORD_LENGTH` adalah panjang maksimum untuk nama pengguna (username) dan kata sandi (password) pengguna.
   - `base_path` adalah direktori root dari file sistem virtual yang akan dibuat.
   - `password_file` adalah path ke file yang berisi informasi pengguna terdaftar.

2. Mendefinisikan Struktur Data Pengguna
   - `struct user` adalah struktur data yang menyimpan informasi pengguna seperti nama pengguna dan kata sandi.

3. Membuat Fungsi Utilitas
   - `is_user_registered(const char* username)`: Memeriksa apakah pengguna dengan nama pengguna yang diberikan sudah terdaftar atau belum.
   - `verify_credentials(const char* username, const char* password)`: Memverifikasi kredensial pengguna dengan memeriksa nama pengguna dan kata sandi yang diberikan.
   - `load_users()`: Membaca file `password.txt` dan menginisialisasi struktur data pengguna.
   - `save_users()`: Menyimpan informasi pengguna yang ada ke dalam file `password.txt`.
   - `add_user(const char* username, const char* password)`: Menambahkan pengguna baru ke dalam struktur data pengguna dan menyimpannya ke file `password.txt`.

4. Mendefinisikan Fungsi-fungsi FUSE
   - `rahasia_getattr(const char* path, struct stat* stbuf)`: Mendapatkan atribut dari sebuah path dalam file sistem virtual.
   - `rahasia_readdir(const char* path, void* buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info* fi)`: Membaca direktori pada path tertentu dalam file sistem virtual dan mengisi entri direktori ke dalam buffer.
   - `rahasia_rename(const char* from, const char* to)`: Mengganti nama (rename) file atau direktori dalam file sistem virtual.

5. Mendefinisikan Operasi FUSE
   - `struct fuse_operations rahasia_oper`: Mendefinisikan operasi-operasi FUSE yang akan digunakan.

6. Fungsi Utama (Main)
   - `load_users()` dipanggil untuk membaca informasi pengguna dari file `password.txt`.
   - Jika argumen baris perintah `--register` diberikan, fungsi `add_user()` dipanggil untuk menambahkan pengguna baru.
   - `fuse_main()` digunakan untuk menjalankan file sistem virtual menggunakan operasi-operasi FUSE yang telah didefinisikan sebelumnya.

Dengan menjalankan program ini, file sistem virtual akan dapat diakses melalui path yang ditentukan oleh `base_path`. Pengguna yang terdaftar dalam file `password.txt` dapat menggunakan nama pengguna dan kata sandi mereka untuk mengakses file sistem virtual tersebut.

Harap diperhatikan bahwa ini hanya gambaran umum tentang cara kerja kode tersebut. Untuk pemahaman yang lebih mendalam, perlu mempelajari dokumentasi FUSE dan

### Screenshot File
![Semantic description of image](/Foto/File3.png "Image Title")

### Screenshot Kode
![Semantic description of image](/Foto/Kode3_1.png "Image Title")
![Semantic description of image](/Foto/Kode3_2.png "Image Title")
![Semantic description of image](/Foto/Kode3_3.png "Image Title")
![Semantic description of image](/Foto/Kode3_4.png "Image Title")
![Semantic description of image](/Foto/Kode3_5.png "Image Title")


### Screenshot Program
![Semantic description of image](/Foto/5.png "Image Title")

### Kendala Pengerjaan
- tidak bisa run program karena docker masih belum terpasang

